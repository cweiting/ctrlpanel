/*********************
*   PID.c
*   simple pid library 
*   pgaskell
**********************/

#include "PID.h"
#include <stdio.h>
#include <stdlib.h>


#define MAX_OUTPUT 1.0
#define MIN_OUTPUT -1.0
#define ITERM_MIN -1.0
#define ITERM_MAX 1.0

PID_t * PID_Init(float Kp, float Ki, float Kd) {
  // allocate memory for PID
  PID_t *pid =  malloc(sizeof(PID_t));

  //initalize values
  pid->pidInput = 0;
  pid->pidOutput = 0;
  pid->pidSetpoint = 0;
  pid->ITerm = 0;
  pid->prevInput = 0;

  // set update rate to 100000 us (0.1s)
  //IMPLEMENT ME!
  pid->updateRate = 100000;
  
  
  //set output limits, integral limits, tunings
  printf("initializing pid...\r\n");
  //IMPLEMENT ME!
  pid->outputMin = MIN_OUTPUT;
  pid->outputMax = MAX_OUTPUT;
  pid->ITermMin = ITERM_MIN;
  pid->ITermMax = ITERM_MAX;

  pid->kp = kp;
  pid->ki = ki;
  pid->kd = kd;

  return pid;
}

void PID_Compute(PID_t* pid) {    
  // Compute PID 
  //IMPLEMENT ME!
  pid->ITerm += pid->pidInput;
  if (pid->ITerm > pid->ITermMax)
  {
    pid->ITerm = pid->ITermMax;
  }
  if (pid->ITerm < pid->ITermMin)
  {
    pid->ITerm = pid->ITermMin;
  }
  pid->output = pid->kp*(pid->pidInput) + pid->ki*(pid->ITerm)*updateRate + pid->kd*(pid->pidInput - pid->prevInput)/updateRate;
  pid->prevInput = pid->pidInput;
}

void PID_SetTunings(PID_t* pid, float Kp, float Ki, float Kd) {
  //scale gains by update rate in seconds for proper units
  float updateRateInSec = ((float) pid->updateRate / 1000000.0);
  //set gains in PID struct
  //IMPLEMENT ME!
  //pid->kp = kp*updateRateInSec;
  pid->ki = ki/updateRateInSec;
  pid->kd = kd*updateRateInSec;
}

void PID_SetOutputLimits(PID_t* pid, float min, float max){
  //set output limits in PID struct
  //IMPLEMENT ME!
  pid->outputMin = min;
  pid->outputMax = max;
}

void PID_SetIntegralLimits(PID_t* pid, float min, float max){
  //set integral limits in PID struct
  //IMPLEMENT ME!
  pid->ITermMin = min;
  pid->ITermMax = max;

}

void PID_SetUpdateRate(PID_t* pid, int updateRate){
  //set integral limits in PID struct
  //IMPLEMENT ME!
  pid->updateRate = updateRate;
}




